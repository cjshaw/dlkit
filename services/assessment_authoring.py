"""DLKit Services implementations of assessment.authoring service."""
# pylint: disable=no-init
#     osid specification includes some 'marker' interfaces.
# pylint: disable=too-many-ancestors
#     number of ancestors defined in spec.
# pylint: disable=too-few-public-methods,too-many-public-methods
#     number of methods defined in spec. Worse yet, these are aggregates.
# pylint: disable=invalid-name
#     method and class names defined in spec.
# pylint: disable=no-self-use,unused-argument
#     to catch unimplemented methods.
# pylint: disable=super-init-not-called
#     it just isn't.



from . import osid
from .osid_errors import Unimplemented, IllegalState
from dlkit.manager_impls.assessment_authoring import managers as assessment_authoring_managers
from dlkit.services.assessment import Bank



DEFAULT = 0
COMPARATIVE = 0
PLENARY = 1
FEDERATED = 0
ISOLATED = 1
AUTOMATIC = 0
MANDATORY = 1
DISABLED = -1


class AssessmentAuthoringProfile(osid.OsidProfile, assessment_authoring_managers.AssessmentAuthoringProfile):
    """AssessmentAuthoringProfile convenience adapter including related Session methods."""


    def __init__(self):
        self._provider_manager = None
    def supports_assessment_part_lookup(self):
        """Pass through to provider supports_assessment_part_lookup"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.supports_resource_lookup
        return self._provider_manager.supports_assessment_part_lookup()

    def supports_assessment_part_admin(self):
        """Pass through to provider supports_assessment_part_admin"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.supports_resource_lookup
        return self._provider_manager.supports_assessment_part_admin()

    def supports_sequence_rule_lookup(self):
        """Pass through to provider supports_sequence_rule_lookup"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.supports_resource_lookup
        return self._provider_manager.supports_sequence_rule_lookup()

    def supports_sequence_rule_admin(self):
        """Pass through to provider supports_sequence_rule_admin"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.supports_resource_lookup
        return self._provider_manager.supports_sequence_rule_admin()

    def get_assessment_part_record_types(self):
        """Pass through to provider get_assessment_part_record_types"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.get_resource_record_types
        return self._provider_manager.get_assessment_part_record_types()

    assessment_part_record_types = property(fget=get_assessment_part_record_types)

    def get_assessment_part_search_record_types(self):
        """Pass through to provider get_assessment_part_search_record_types"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.get_resource_record_types
        return self._provider_manager.get_assessment_part_search_record_types()

    assessment_part_search_record_types = property(fget=get_assessment_part_search_record_types)

    def get_sequence_rule_record_types(self):
        """Pass through to provider get_sequence_rule_record_types"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.get_resource_record_types
        return self._provider_manager.get_sequence_rule_record_types()

    sequence_rule_record_types = property(fget=get_sequence_rule_record_types)

    def get_sequence_rule_search_record_types(self):
        """Pass through to provider get_sequence_rule_search_record_types"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.get_resource_record_types
        return self._provider_manager.get_sequence_rule_search_record_types()

    sequence_rule_search_record_types = property(fget=get_sequence_rule_search_record_types)

    def get_sequence_rule_enabler_record_types(self):
        """Pass through to provider get_sequence_rule_enabler_record_types"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.get_resource_record_types
        return self._provider_manager.get_sequence_rule_enabler_record_types()

    sequence_rule_enabler_record_types = property(fget=get_sequence_rule_enabler_record_types)

    def get_sequence_rule_enabler_search_record_types(self):
        """Pass through to provider get_sequence_rule_enabler_search_record_types"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProfile.get_resource_record_types
        return self._provider_manager.get_sequence_rule_enabler_search_record_types()

    sequence_rule_enabler_search_record_types = property(fget=get_sequence_rule_enabler_search_record_types)


class AssessmentAuthoringManager(osid.OsidManager, osid.OsidSession, AssessmentAuthoringProfile, assessment_authoring_managers.AssessmentAuthoringManager):
    """AssessmentAuthoringManager convenience adapter including related Session methods."""


    def __init__(self, proxy=None):
        self._runtime = None
        self._provider_manager = None
        self._provider_sessions = dict()
        self._session_management = AUTOMATIC
        self._bank_view = DEFAULT
        # This is to initialize self._proxy
        osid.OsidSession.__init__(self, proxy)


    # def _get_view(self, view):
    #     """Gets the currently set view"""
    #     if view in self._views:
    #         return self._views[view]
    #     else:
    #         self._views[view] = DEFAULT
    #         return DEFAULT


    def _set_bank_view(self, session):
        """Sets the underlying bank view to match current view"""
        if self._bank_view == COMPARATIVE:
            try:
                session.use_comparative_bank_view()
            except AttributeError:
                pass
        else:
            try:
                session.use_plenary_bank_view()
            except AttributeError:
                pass


    def _get_provider_session(self, session_name, proxy=None):
        """Gets the session for the provider"""
        if self._proxy is None:
            self._proxy = proxy
        if session_name in self._provider_sessions:
            return self._provider_sessions[session_name]
        else:
            session = self._instantiate_session('get_' + session_name, self._proxy)
            self._set_bank_view(session)
            if self._session_management != DISABLED:
                self._provider_sessions[session_name] = session
            return session


    def _instantiate_session(self, method_name, proxy=None, *args, **kwargs):
        """Instantiates a provider session"""
        session_class = getattr(self._provider_manager, method_name)
        if proxy is None:
            return session_class(*args, **kwargs)
        else:
            return session_class(proxy=proxy, *args, **kwargs)


    def initialize(self, runtime):
        """OSID Manager initialize"""
        from .primitives import Id
        if self._runtime is not None:
            raise IllegalState('Manager has already been initialized')
        self._runtime = runtime
        config = runtime.get_configuration()
        parameter_id = Id('parameter:assessment_authoringProviderImpl@dlkit_service')
        provider_impl = config.get_value_by_parameter(parameter_id).get_string_value()
        if self._proxy is None:
            # need to add version argument
            self._provider_manager = runtime.get_manager('ASSESSMENT_AUTHORING', provider_impl)
        else:
            # need to add version argument
            self._provider_manager = runtime.get_proxy_manager('ASSESSMENT_AUTHORING', provider_impl)


    def close_sessions(self):
        """Close all sessions, unless session management is set to MANDATORY"""
        if self._session_management != MANDATORY:
            self._provider_sessions = dict()


    def use_automatic_session_management(self):
        """Session state will be saved unless closed by consumers"""
        self._session_management = AUTOMATIC


    def use_mandatory_session_management(self):
        """Session state will be saved and can not be closed by consumers"""
        self._session_management = MANDATORY


    def disable_session_management(self):
        """Session state will never be saved"""
        self._session_management = DISABLED
        self.close_sessions()
    def get_assessment_part_lookup_session(self, *args, **kwargs):
        """Pass through to provider get_assessment_part_lookup_session"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_catalog_template
        session = self._instantiate_session(method_name='get_assessment_part_lookup_session', proxy=self._proxy, *args, **kwargs)
        return Bank(
            self._provider_manager,
            session.get_bank(),
            self._proxy, assessment_part_lookup_session=session)

    assessment_part_lookup_session = property(fget=get_assessment_part_lookup_session)

    def get_assessment_part_lookup_session_for_bank(self, *args, **kwargs):
        """Pass through to provider get_assessment_part_lookup_session_for_bank"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_for_bin_catalog_template
        if self._proxy:
            session = self._provider_manager.get_assessment_part_lookup_session_for_bank(proxy=self._proxy, *args, **kwargs)
        else:
            session = self._provider_manager.get_assessment_part_lookup_session_for_bank(*args, **kwargs)
        return Bank(
            self._provider_manager,
            self.get_bank(*args, **kwargs),
            self._proxy,
            assessment_part_lookup_session=session)

    def get_assessment_part_admin_session(self, *args, **kwargs):
        """Pass through to provider get_assessment_part_admin_session"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_catalog_template
        session = self._instantiate_session(method_name='get_assessment_part_admin_session', proxy=self._proxy, *args, **kwargs)
        return Bank(
            self._provider_manager,
            session.get_bank(),
            self._proxy, assessment_part_admin_session=session)

    assessment_part_admin_session = property(fget=get_assessment_part_admin_session)

    def get_assessment_part_admin_session_for_bank(self, *args, **kwargs):
        """Pass through to provider get_assessment_part_admin_session_for_bank"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_for_bin_catalog_template
        if self._proxy:
            session = self._provider_manager.get_assessment_part_admin_session_for_bank(proxy=self._proxy, *args, **kwargs)
        else:
            session = self._provider_manager.get_assessment_part_admin_session_for_bank(*args, **kwargs)
        return Bank(
            self._provider_manager,
            self.get_bank(*args, **kwargs),
            self._proxy,
            assessment_part_admin_session=session)

    def get_sequence_rule_lookup_session(self, *args, **kwargs):
        """Pass through to provider get_sequence_rule_lookup_session"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_catalog_template
        session = self._instantiate_session(method_name='get_sequence_rule_lookup_session', proxy=self._proxy, *args, **kwargs)
        return Bank(
            self._provider_manager,
            session.get_bank(),
            self._proxy, sequence_rule_lookup_session=session)

    sequence_rule_lookup_session = property(fget=get_sequence_rule_lookup_session)

    def get_sequence_rule_lookup_session_for_bank(self, *args, **kwargs):
        """Pass through to provider get_sequence_rule_lookup_session_for_bank"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_for_bin_catalog_template
        if self._proxy:
            session = self._provider_manager.get_sequence_rule_lookup_session_for_bank(proxy=self._proxy, *args, **kwargs)
        else:
            session = self._provider_manager.get_sequence_rule_lookup_session_for_bank(*args, **kwargs)
        return Bank(
            self._provider_manager,
            self.get_bank(*args, **kwargs),
            self._proxy,
            sequence_rule_lookup_session=session)

    def get_sequence_rule_admin_session(self, *args, **kwargs):
        """Pass through to provider get_sequence_rule_admin_session"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_catalog_template
        session = self._instantiate_session(method_name='get_sequence_rule_admin_session', proxy=self._proxy, *args, **kwargs)
        return Bank(
            self._provider_manager,
            session.get_bank(),
            self._proxy, sequence_rule_admin_session=session)

    sequence_rule_admin_session = property(fget=get_sequence_rule_admin_session)

    def get_sequence_rule_admin_session_for_bank(self, *args, **kwargs):
        """Pass through to provider get_sequence_rule_admin_session_for_bank"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceManager.get_resource_lookup_session_for_bin_catalog_template
        if self._proxy:
            session = self._provider_manager.get_sequence_rule_admin_session_for_bank(proxy=self._proxy, *args, **kwargs)
        else:
            session = self._provider_manager.get_sequence_rule_admin_session_for_bank(*args, **kwargs)
        return Bank(
            self._provider_manager,
            self.get_bank(*args, **kwargs),
            self._proxy,
            sequence_rule_admin_session=session)


class AssessmentAuthoringProxyManager(osid.OsidProxyManager, AssessmentAuthoringProfile, assessment_authoring_managers.AssessmentAuthoringProxyManager):
    """AssessmentAuthoringProxyManager convenience adapter including related Session methods."""

    def get_assessment_part_lookup_session(self, *args, **kwargs):
        """Sends control to Manager"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProxyManager.get_resource_lookup_session_template
        return AssessmentAuthoringManager.get_assessment_part_lookup_session(*args, **kwargs)

    def get_assessment_part_lookup_session_for_bank(self, *args, **kwargs):
        """Sends control to Manager"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProxyManager.get_resource_lookup_session_for_bin_template
        return AssessmentAuthoringManager.get_assessment_part_lookup_session_for_bank(*args, **kwargs)

    def get_assessment_part_admin_session(self, *args, **kwargs):
        """Pass through to provider unimplemented"""
        raise Unimplemented('Unimplemented in dlkit.services - args=' + str(args) + ', kwargs=' + str(kwargs))

    def get_assessment_part_admin_session_for_bank(self, *args, **kwargs):
        """Pass through to provider unimplemented"""
        raise Unimplemented('Unimplemented in dlkit.services - args=' + str(args) + ', kwargs=' + str(kwargs))

    def get_sequence_rule_lookup_session(self, *args, **kwargs):
        """Sends control to Manager"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProxyManager.get_resource_lookup_session_template
        return AssessmentAuthoringManager.get_sequence_rule_lookup_session(*args, **kwargs)

    def get_sequence_rule_lookup_session_for_bank(self, *args, **kwargs):
        """Sends control to Manager"""
        # Implemented from kitosid template for -
        # osid.resource.ResourceProxyManager.get_resource_lookup_session_for_bin_template
        return AssessmentAuthoringManager.get_sequence_rule_lookup_session_for_bank(*args, **kwargs)

    def get_sequence_rule_admin_session(self, *args, **kwargs):
        """Pass through to provider unimplemented"""
        raise Unimplemented('Unimplemented in dlkit.services - args=' + str(args) + ', kwargs=' + str(kwargs))

    def get_sequence_rule_admin_session_for_bank(self, *args, **kwargs):
        """Pass through to provider unimplemented"""
        raise Unimplemented('Unimplemented in dlkit.services - args=' + str(args) + ', kwargs=' + str(kwargs))


