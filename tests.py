"""DLkit tests"""
# pylint: skip-file
#    This is ugly and will all be replaced with generated tests

import pdb

import unittest
import random
from . import utilities
#from . import settings
from services.primitives import *
from abstract_osid.osid.errors import AlreadyExists, NotFound, OperationFailed, IllegalState
from services.learning import LearningManager
from services.repository import RepositoryManager
from services.relationship import RelationshipManager
#from .assessment.managers import AssessmentManager, AssessmentProxyManager
from services.proxy import ProxyManager
from services.type import TypeManager
from .abstract_osid.learning import sessions as abc_learning_sessions
from .abstract_osid.learning import objects as abc_learning_objects
from .abstract_osid.repository import objects as abc_repository_objects
from .abstract_osid.type import primitives as abc_type_primitives
from .abstract_osid.id import primitives as abc_id_primitives
from .abstract_osid.id import objects as abc_id_objects
#from .proxy_example import TestRequest

lm = LearningManager()
rm = RepositoryManager()
relm = RelationshipManager()
tm = TypeManager()

DEFAULT_TYPE = Type(**{
    'authority': 'this.is.a.bogus.authority',
    'namespace': 'this.is.a.bogus.namespace',
    'identifier': 'mc3-objectivebank%3Amc3.learning.objectivebank.sandbox%40MIT-OEIT',
    'domain': '',
    'display_name': '',
    'description': '',
    'display_label': ''
    })

def objective_bank_exists(name):
    test = False
    all_banks = lm.get_objective_banks()
    for bank in all_banks:
        if bank.display_name.text == name:
            test = True
    return test

def repository_exists(name):
    test = False
    all_repos = rm.get_repositories()
    for repo in all_repos:
        if repo.display_name.text == name:
            test = True
    return test

class TestLearningManager(unittest.TestCase):

    def test_get_display_name(self):
        self.assertTrue(isinstance(
                lm.get_display_name().get_text(), str))

    def test_get_description(self):
        self.assertTrue(isinstance(
                lm.get_description().get_text(), str))

    def test_get_profile_language_type(self):
        self.assertTrue(isinstance(
                lm.get_display_name().get_language_type(), 
                abc_type_primitives.Type))

    def test_get_profile_script_type(self):
        self.assertTrue(isinstance(
                lm.get_display_name().get_script_type(), 
                abc_type_primitives.Type))

    def test_get_profile_format_type(self):
        self.assertTrue(isinstance(
                lm.get_display_name().get_format_type(), 
                abc_type_primitives.Type))

    def test_display_name_turtles(self):
        self.assertTrue(isinstance(
                lm.get_display_name().get_language_type().get_display_name().get_script_type().get_display_label().get_text(),
                basestring))

    def test_supports_objective_bank_lookup(self):
        self.assertTrue(lm.supports_objective_bank_lookup())

    def test_supports_objective_lookup(self):
        self.assertTrue(lm.supports_objective_lookup())

    def test_supports_objective_query(self):
        self.assertFalse(lm.supports_objective_query())

    def test_get_objective_bank_lookup_session(self):
        obls = lm.get_objective_bank_lookup_session()
        #self.assertTrue(isinstance(obls, 
        #                abc_learning_sessions.ObjectiveBankLookupSession))

    def test_get_objective_lookup_session(self):
        ols = lm.get_objective_bank_lookup_session()
        #self.assertTrue(isinstance(ols, 
        #                abc_learning_sessions.ObjectiveBankLookupSession))

    def test_get_objective_lookup_session_for_objective_bank(self):
        obls = lm.get_objective_bank_lookup_session()
        ob = obls.objective_banks.next()
        #ols = lm.get_objective_lookup_session()
        ols_for_ob = lm.get_objective_lookup_session_for_objective_bank(ob.ident)
        self.assertTrue(ob.ident.get_identifier() == 
                        ols_for_ob.get_objective_bank_id().get_identifier())


class TestObjectiveBankLookup(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    def setUp(self):
        pass

    def test_get_objective_banks(self):
        all_banks = lm.get_objective_banks()
        self.assertTrue(all_banks.available() > 0)

    def test_get_objective_bank(self):
        all_banks = lm.get_objective_banks()
        self.assertTrue(all_banks.available() > 0)
        for bank in all_banks:
            self.assertTrue(isinstance(bank, abc_learning_objects.ObjectiveBank))

    def test_get_objective_banks_by_ids(self):
        all_banks = lm.get_objective_banks()
        all_banks_count = all_banks.available()
        id_list = []
        for bank in all_banks:
            id_list.append(bank.get_id())
        self.assertTrue(len(id_list) == all_banks_count)
        all_banks_by_ids = lm.get_objective_banks_by_ids(id_list)
        self.assertTrue(all_banks_by_ids.available() == all_banks_count)
        for bank in all_banks_by_ids:
            self.assertTrue(isinstance(bank, abc_learning_objects.ObjectiveBank))

"""
class TestIdEquality(unittest.TestCase):
    def setUp(self):
        self.equiv_id1 = utilities.get_objective_bank_by_name('Second Test Objective Bank').ident
        self.equiv_id2 = utilities.get_objective_bank_by_name('Second Test Objective Bank').ident
        self.non_equiv_id = utilities.get_objective_bank_by_name('Third Objective Bank').ident

    def test_equivelant_ids(self):
        self.assertTrue(isinstance(self.equiv_id1, Id))
        self.assertTrue(isinstance(self.equiv_id2, Id))
        self.assertEqual(self.equiv_id1, self.equiv_id2)

    def test_non_equivelant_ids(self):
        self.assertTrue(isinstance(self.equiv_id1, Id))
        self.assertTrue(isinstance(self.non_equiv_id, Id))
        self.assertNotEqual(self.equiv_id1, self.non_equiv_id)

    def test_list_inclusion(self):
        id_list = [self.equiv_id1, self.non_equiv_id]
        self.assertTrue(self.equiv_id2 in id_list)
"""


class TestObjectiveLookup(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_lookup(),
                     "objective lookup not supported in any underlying implementation")
    @unittest.skipIf(not objective_bank_exists('Crosslinks'),
                     "Crosslinks bank not found in any underlying implementation")
    def setUp(self):
        self.test_bank = None
        all_banks = lm.get_objective_banks()
        for bank in all_banks:
            if bank.display_name.text == 'Crosslinks':
                self.test_bank = bank

    def test_get_objective_bank(self):
        test_bank_id = self.test_bank.get_objective_bank().get_id().get_identifier()
        self.assertTrue(test_bank_id == self.test_bank.get_id().get_identifier())

    def test_get_objectives(self):
        objectives = self.test_bank.get_objectives()
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_get_objective(self):
        # Get a random Objective
        all_objectives = self.test_bank.get_objectives()
        all_objectives.skip(random.randint(0, all_objectives.available() - 1))
        rand_objective = all_objectives.get_next_objective()
        objective = self.test_bank.get_objective(rand_objective.get_id())
        self.assertTrue(objective.get_id().get_identifier() == 
                        rand_objective.get_id().get_identifier())

    def test_get_objectives_by_type(self):
        # Get a random Objective type from bank
        all_objectives = self.test_bank.get_objectives()
        all_objectives.skip(random.randint(0, all_objectives.available() - 1))
        rand_type = all_objectives.get_next_objective().get_genus_type()
        objectives = self.test_bank.get_objectives_by_genus_type(rand_type)
        for objective in objectives:
            self.assertTrue(objective.get_genus_type().get_identifier() == rand_type.get_identifier())

    def test_objective_is_of_genus_type(self):
        # Get a random Objective type from bank
        all_objectives = self.test_bank.get_objectives()
        all_objectives.skip(random.randint(0, all_objectives.available() - 1))
        rand_type = all_objectives.get_next_objective().get_genus_type()
        objectives = self.test_bank.get_objectives_by_genus_type(rand_type)
        for objective in objectives:
            self.assertTrue(objective.is_of_genus_type(rand_type))


class TestObjectiveRequisite(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_requisite(),
                     "objective requisite not supported in any underlying implementation")
    @unittest.skipIf(not objective_bank_exists('Crosslinks'),
                     "Crosslinks bank not found in any underlying implementation")
    def setUp(self):
        self.test_bank = utilities.get_objective_bank_by_name('Crosslinks')
        test_objective = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Definite integral')
        self.test_objective_copy_id = Id(
                            authority = test_objective.ident.authority,
                            namespace = test_objective.ident.namespace,
                            identifier = test_objective.ident.identifier)
        self.test_objective_id = test_objective.ident      
        req_objective = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Summation')
        self.req_objective_copy_id = Id(
                            authority = req_objective.ident.authority,
                            namespace = req_objective.ident.namespace,
                            identifier = req_objective.ident.identifier)
        self.req_objective_id = req_objective.ident

    def test_get_requisite_objectives(self):
        objectives = self.test_bank.get_requisite_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_get_all_requisite_objectives(self):
        objectives = self.test_bank.get_all_requisite_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_get_dependent_objectives(self):
        objectives = self.test_bank.get_dependent_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_is_objective_required(self):
        self.assertTrue(self.test_bank.is_objective_required(self.test_objective_id,
                                                       self.req_objective_id))

    def test_is_objective_required_copy(self):
        self.assertTrue(self.test_bank.is_objective_required(self.test_objective_copy_id,
                                                       self.req_objective_copy_id))

    def test_objective_id_copy(self):
        self.assertTrue(self.test_objective_id == self.test_objective_copy_id)

    def test_req_objective_id_copy(self):
        self.assertTrue(self.req_objective_id == self.req_objective_copy_id)

    def test_get_equivalent_objectives(self):
        # Don't know how to test this yet.  Probably need to replace all of the
        # Requisite tests with more of a CRuD pattern
        self.assertTrue(True)
        
class TestObjectiveHierarchy(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_hierarchy(),
                     "objective hierarchy not supported in any underlying implementation")
    @unittest.skipIf(not objective_bank_exists('Chemistry Bridge'),
                     "Crosslinks bank not found in any underlying implementation")
    def setUp(self):
        self.test_bank = utilities.get_objective_bank_by_name('Chemistry Bridge')
        self.test_objective = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Acids and Bases')
        self.test_objective_id = self.test_objective.ident
        self.test_parent = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Buffers: A study of Chemical Equilibria')
        self.test_parent_id = self.test_parent.ident
        self.test_child = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Strength of an acid/base')
        self.test_child_id = self.test_child.ident
        self.test_final_child = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Auto-Ionization of Water outcome 3D1')
        self.test_final_child_id = self.test_final_child.ident


    def test_get_root_objective_ids(self):
        objective_ids = self.test_bank.get_root_objective_ids()
        self.assertTrue(isinstance(objective_ids, abc_id_objects.IdList))
        for objective_id in objective_ids:
            self.assertTrue(isinstance(objective_id, abc_id_primitives.Id))

    def test_get_root_objectives(self):
        objectives = self.test_bank.get_root_objectives()
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_has_parent_objectives(self):
        self.assertTrue(self.test_bank.has_parent_objectives(self.test_objective_id))
        self.assertFalse(self.test_bank.has_parent_objectives(self.test_parent_id))

    def test_is_parent_of_objective(self):
        self.assertTrue(self.test_bank.is_parent_of_objective(self.test_parent_id,
                                                       self.test_objective_id))
        self.assertFalse(self.test_bank.is_parent_of_objective(self.test_child_id,
                                                       self.test_objective_id))

    def test_get_parent_objective_ids(self):
        objective_ids = self.test_bank.get_parent_objective_ids(self.test_objective_id)
        self.assertTrue(isinstance(objective_ids, abc_id_objects.IdList))
        for objective_id in objective_ids:
            self.assertTrue(isinstance(objective_id, abc_id_primitives.Id))

    def test_get_parent_objectives(self):
        objectives = self.test_bank.get_parent_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_has_child_objectives(self):
        self.assertTrue(self.test_bank.has_child_objectives(self.test_objective_id))
        self.assertFalse(self.test_bank.has_child_objectives(self.test_final_child_id))

    def test_is_child_of_objective(self):
        self.assertTrue(self.test_bank.is_child_of_objective(self.test_child_id,
                                                       self.test_objective_id))
        self.assertFalse(self.test_bank.is_child_of_objective(self.test_parent_id,
                                                       self.test_objective_id))

    def test_get_child_objective_ids(self):
        objective_ids = self.test_bank.get_child_objective_ids(self.test_objective_id)
        self.assertTrue(isinstance(objective_ids, abc_id_objects.IdList))
        for objective_id in objective_ids:
            self.assertTrue(isinstance(objective_id, abc_id_primitives.Id))

    def test_get_child_objectives(self):
        objectives = self.test_bank.get_child_objectives(self.test_child_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

class TestActivityLookup(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_activity_lookup(),
                     "activity lookup not supported in any underlying implementation")
    @unittest.skipIf(not objective_bank_exists('Crosslinks'),
                     "Crosslinks bank not found in any underlying implementation")
    def setUp(self):
        all_banks = lm.get_objective_banks()
        for bank in all_banks:
            if bank.display_name.text == 'Crosslinks':
                self.test_bank = bank

    def test_get_objective_bank(self):
        test_bank_id = self.test_bank.get_objective_bank().get_id().get_identifier()
        self.assertTrue(test_bank_id == self.test_bank.get_id().get_identifier())

    def test_get_activities(self):
        activities = self.test_bank.get_activities()
        self.assertTrue(isinstance(activities, abc_learning_objects.ActivityList))
        for activity in activities:
            self.assertTrue(isinstance(activity, abc_learning_objects.Activity))

    def test_get_activity(self):
        # Get a random Activity
        all_activities = self.test_bank.get_activities()
        all_activities.skip(random.randint(0, all_activities.available() - 1))
        rand_activity = all_activities.get_next_activity()
        activity = self.test_bank.get_activity(rand_activity.get_id())
        self.assertTrue(activity.get_id().get_identifier() == 
                        rand_activity.get_id().get_identifier())

    def test_get_activities_by_type(self):
        # Get a random Activity type from Activities in Bank
        all_activities = self.test_bank.get_activities()
        all_activities.skip(random.randint(0, all_activities.available() - 1))
        rand_type = all_activities.get_next_activity().get_genus_type()
        activities = self.test_bank.get_activities_by_genus_type(rand_type)
        for activity in activities:
            self.assertTrue(activity.get_genus_type().get_identifier() == rand_type.get_identifier())

    def test_activity_is_of_genus_type(self):
        # Get a random Activity type from Activities in Bank
        all_activities = self.test_bank.get_activities()
        all_activities.skip(random.randint(0, all_activities.available() - 1))
        rand_type = all_activities.get_next_activity().get_genus_type()
        activities = self.test_bank.get_activities_by_genus_type(rand_type)
        for activity in activities:
            self.assertTrue(activity.is_of_genus_type(rand_type))


class TestObjectiveMetadata(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objecttive bank lookup or objective admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_bank_admin(),
                     "objective bank admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_admin(),
                     "objective admin not supported in any underlying implementation")
    def setUp(self):
        self.obls = lm.get_objective_bank_lookup_session()
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        self.objective_form = self.test_bank.get_objective_form_for_create([])

    def test_get_journal_comment_metadata(self):
        cm = self.objective_form.get_journal_comment_metadata()
        self.assertEqual(cm.get_syntax(), 'STRING')

    def test_get_display_name_metadata(self):
        cm = self.objective_form.get_display_name_metadata()
        self.assertEqual(cm.get_syntax(), 'STRING')

    def test_get_description_metadata(self):
        cm = self.objective_form.get_description_metadata()
        self.assertEqual(cm.get_syntax(), 'STRING')

    def test_get_genus_type_metadata(self):
        cm = self.objective_form.get_genus_type_metadata()
        self.assertEqual(cm.get_syntax(), 'TYPE')

    def test_get_assessment_metadata(self):
        cm = self.objective_form.get_assessment_metadata()
        self.assertEqual(cm.get_syntax(), 'ID')

    def test_get_cognitive_process_metadata(self):
        cm = self.objective_form.get_cognitive_process_metadata()
        self.assertEqual(cm.get_syntax(), 'ID')

    def test_get_knowledge_category_metadata(self):
        cm = self.objective_form.get_knowledge_category_metadata()
        self.assertEqual(cm.get_syntax(), 'ID')


class TestTypeCrUD(unittest.TestCase):

    @unittest.skipIf(not tm.supports_type_lookup(),
                     "type lookup not supported in any underlying implementation")
    @unittest.skipIf(not tm.supports_type_admin(),
                     "type admin not supported in any underlying implementation")
    def setUp(self):
        self.test_type = Type(**{
            'authority': 'test-authority',
            'namespace': 'test-namespace',
            'identifier': 'test-identifier',
            'display_name': 'CRuD-Test Type',
            'display_label': 'CRuD',
            'description': 'This is a Test Type for CRuD',
            'domain': 'Test Domain'
        })

    def test_crud_types(self):
        # test create:
        if not tm.has_type(self.test_type):
            tfc = tm.get_type_form_for_create(self.test_type)
            tfc.display_name = 'CRuD-Test Type'
            tfc.display_label = 'CRuD-Test'
            tfc.description = 'This is a Test Type for CRuD'
            tfc.domain = 'Test Domain'
            new_type = tm.create_type(tfc)
        else:
            new_type = tm.get_type(self.test_type)
        self.assertEqual(new_type.display_name.text, 'CRuD-Test Type')
        self.assertEqual(new_type.display_label.text, 'CRuD-Test')
        self.assertEqual(new_type.description.text, 'This is a Test Type for CRuD')
        self.assertEqual(new_type.domain.text, 'Test Domain')

       # test update:
        tfu = tm.get_type_form_for_update(new_type)
        tfu.set_display_name('New Name for CRuD-Test Type')
        tm.update_type(tfu)
        updated_type_id = {'authority': new_type.get_authority(),
                           'namespace': new_type.get_identifier_namespace(),
                           'identifier': new_type.get_identifier()}
        updated_type = tm.get_type(**updated_type_id)
        ## THIS FAILS, But why?
        #self.assertEqual(updated_type.display_name.text, 'New Name for CRuD-Test Type')

        # test delete:
        tm.delete_type(updated_type)
        with self.assertRaises(NotFound):
            tm.get_type({'authority': updated_type.get_authority(),
                         'namespace': updated_type.get_identifier_namespace(),
                         'identifier': updated_type.get_identifier()})

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        try:
            tm.delete_type(self.test_type)
        except:
            print 'Could not delete test type'


class TestObjectiveBankCrUD(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_admin(),
                     "objective bank admin not supported in any underlying implementation")
    def setUp(self):
        self.default_bank_type = DEFAULT_TYPE

    def test_crud_objective_banks(self):
        # test create:
        obfc = lm.get_objective_bank_form([])
        obfc.display_name = 'CRuD-Test Objective Bank'
        obfc.description = 'This is a Test Objective Bank for CRuD'
        new_objective_bank = lm.create_objective_bank(obfc)
        self.assertEqual(new_objective_bank.display_name.text, 'CRuD-Test Objective Bank')

        # test update:
        new_objective_bank_id = new_objective_bank.get_id()
        obfu = lm.get_objective_bank_form(new_objective_bank_id)
        obfu.set_display_name('Even Newer Name for CRuD-Test Objective Bank')
        lm.update_objective_bank(obfu)
        updated_objective_bank_id = new_objective_bank_id
        self.assertEqual(lm.get_objective_bank(updated_objective_bank_id).display_name.text, 'Even Newer Name for CRuD-Test Objective Bank')

        # test delete:
        lm.delete_objective_bank(updated_objective_bank_id)
        with self.assertRaises(NotFound):
            lm.get_objective_bank(updated_objective_bank_id)

    def test_truth(self):
        self.assertTrue(True)

"""
class TestObjectiveBankNotEmptyDelete(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        self.obas = lm.get_objective_bank_admin_session()
        self.obls = lm.get_objective_bank_lookup_session()
        self.default_bank_type = DEFAULT_TYPE

    def test_delete_non_empty_objective_banks(self):
        # test create:
        obfc = self.obas.get_objective_bank_form_for_create([])
        obfc.display_name = 'CRuD-Test Non Empty Objective Bank Delete'
        obfc.description = 'This is a Test for deleting non-empty Objective Banks'
        objective_bank = self.obas.create_objective_bank(obfc)
        objective_bank_id = objective_bank.get_id()
        # This is a little test of ID str round trip:
        objective_bank_idstr = str(objective_bank_id)
        self.assertEqual(objective_bank_id, Id(objective_bank_idstr))
        # End round trip test
        oas = lm.get_objective_admin_session_for_objective_bank(objective_bank_id)
        ofc = oas.get_objective_form_for_create([])
        ofc.set_display_name('Test Objective')
        ofc.set_description('This is a Test Objective')
        objective = oas.create_objective(ofc)
        # test non-empty delete:
        with self.assertRaises(IllegalState):
            self.obas.delete_objective_bank(objective_bank_id)
        # test empty delete:
        oas.delete_objective(objective.get_id())
        self.obas.delete_objective_bank(objective_bank_id)
        with self.assertRaises(NotFound):
            self.obls.get_objective_bank(objective_bank_id)
        

    def test_truth(self):
        self.assertTrue(True)
"""


class TestObjectiveCrUD(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_bank_admin(),
                     "objective bank admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_admin(),
                     "objective admin not supported in any underlying implementation")
    def setUp(self):
        try:
            test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        test_bank_id = test_bank.ident
        self.test_bank = lm.get_objective_bank(test_bank_id)
        self.assertEqual(self.test_bank.get_display_name().get_text(), 'Python Test Sandbox')

    def test_crud_objective(self):
        # test create:
        ofc = self.test_bank.get_objective_form([])
        ofc.set_display_name('Test Objective')
        ofc.set_description('This is a Test Objective')
        new_objective = self.test_bank.create_objective(ofc)
        self.assertEqual(new_objective.get_display_name().get_text(), 'Test Objective')

        # test update:
        new_objective_id = new_objective.get_id()
        #self.test_bank.use_federated_objective_bank_view()
        ofu = self.test_bank.get_objective_form(new_objective_id)
        ofu.set_display_name('New Name for Test Objective')
        self.test_bank.update_objective(ofu)
        updated_objective_id = new_objective_id
        updated_objective = self.test_bank.get_objective(updated_objective_id)
        self.assertEqual(new_objective_id.get_identifier(), updated_objective_id.get_identifier())
        self.assertEqual(self.test_bank.get_objective(updated_objective_id).get_display_name().get_text(), 'New Name for Test Objective')

        # test delete:
        self.test_bank.delete_objective(updated_objective_id)
        with self.assertRaises(NotFound):
            self.test_bank.get_objective(updated_objective_id)

    def test_truth(self):
        self.assertTrue(True)


class TestActivityCrUD(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_bank_admin(),
                     "objective bank admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_admin(),
                     "objective admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_activity_admin(),
                     "activity admin not supported in any underlying implementation")
    def setUp(self):
        obls = lm.get_objective_bank_lookup_session()
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        test_bank_id = self.test_bank.ident
        self.assertEqual(self.test_bank.get_display_name().get_text(), 'Python Test Sandbox')
        ofc = self.test_bank.get_objective_form([])
        ofc.set_display_name('Activity CrUD Test Objective')
        ofc.set_description('This is a Test Objective for testing Activity CrUD')
        self.test_objective = self.test_bank.create_objective(ofc)
        self.test_objective_id = self.test_objective.get_id()

    def test_crud_activities(self):
        # test create:
        afc = self.test_bank.get_activity_form(self.test_objective_id, [])
        afc.set_display_name('Test Activity')
        afc.set_description('This is a Test Activity')
        new_activity = self.test_bank.create_activity(afc)
        self.assertEqual(new_activity.get_display_name().get_text(), 'Test Activity')

        # test update:
        new_activity_id = new_activity.get_id()
        afu = self.test_bank.get_activity_form(new_activity_id)
        afu.set_display_name('New Name for Test Activity')
        self.test_bank.update_activity(afu)
        updated_activity_id = new_activity_id
        updated_activity = self.test_bank.get_activity(updated_activity_id)
        self.assertEqual(new_activity_id.get_identifier(), updated_activity_id.get_identifier())
        self.assertEqual(self.test_bank.get_activity(updated_activity_id).get_display_name().get_text(), 'New Name for Test Activity')

        # test delete
        self.test_bank.delete_activity(updated_activity_id)
        with self.assertRaises(NotFound):
            self.test_bank.get_activity(updated_activity_id)

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        self.test_bank.delete_objective(self.test_objective_id)
        with self.assertRaises(NotFound):
            self.test_bank.get_objective(self.test_objective_id)

class TestObjectiveDesignAndSeq(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_bank_admin(),
                     "objective bank admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_admin(),
                     "objective admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_hierarchy_design(),
                     "objective hierarchy design not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_sequencing(),
                     "objective sequencing not supported in any underlying implementation")
    def setUp(self):
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        self.test_bank_id = self.test_bank.ident
        self.parent_objective = utilities.create_objective(self.test_bank_id, 
            'parent objective', 'parent objective for testing hierarchy design')
        self.child_objectives = list()
        i = 0 
        while i < 5:
            i += 1
            self.child_objectives.append(utilities.create_objective(self.test_bank_id, 
                'child objective ' + str(i), 
                '#' + str(i) + ' child objective for testing hierarchy design'))

    def test_add_remove_child_objective(self):
        for child_objective in self.child_objectives:
            try:
                self.test_bank.add_child_objective(self.parent_objective.ident, 
                                              child_objective.ident)
            except AlreadyExists:
                pass
        self.assertEqual(len(self.test_bank.get_child_objectives(self.parent_objective.ident)), 5)
        self.test_bank.remove_child_objective(self.parent_objective.ident, self.child_objectives[0].ident)
        self.assertEqual(len(self.test_bank.get_child_objectives(self.parent_objective.ident)), 4)
        self.test_bank.move_objective_ahead(self.parent_objective.ident,
                                            self.child_objectives[1].ident,
                                            self.child_objectives[3].ident)
        self.test_bank.move_objective_behind(self.parent_objective.ident,
                                            self.child_objectives[2].ident,
                                            self.child_objectives[3].ident)
        self.test_bank.remove_child_objectives(self.parent_objective.ident)
        self.assertEqual(len(self.test_bank.get_child_objectives(self.parent_objective.ident)), 0)

    def test_five_children_in_list(self):
        self.assertEqual(len(self.child_objectives), 5)


class TestObjectiveRequisiteAssignment(unittest.TestCase):

    @unittest.skipIf(not lm.supports_objective_bank_lookup(),
                     "objective bank lookup not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_bank_admin(),
                     "objective bank admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_admin(),
                     "objective admin not supported in any underlying implementation")
    @unittest.skipIf(not lm.supports_objective_requisite_assignment(),
                     "requisite assignment not supported in any underlying implementation")
    def setUp(self):
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        self.test_bank_id = self.test_bank.ident
        self.dependent_objective = utilities.create_objective(self.test_bank_id, 
            'dependent objective', 'dependent objective for testing requisite assignment')
        self.required_objectives = list()
        i = 0 
        while i < 3:
            i += 1
            self.required_objectives.append(utilities.create_objective(self.test_bank_id, 
                'required objective ' + str(i), 
                '#' + str(i) + ' required objective for testing requisite assignment'))

    def test_requisite_assignment(self):
        for required_objective in self.required_objectives:
            try:
                self.test_bank.assign_objective_requisite(self.dependent_objective.ident, 
                                                          required_objective.ident)
            except AlreadyExists:
                pass
        self.assertEqual(len(self.test_bank.get_requisite_objectives(self.dependent_objective.ident)), 3)
        self.test_bank.unassign_objective_requisite(self.dependent_objective.ident, self.required_objectives[0].ident)
        self.assertEqual(len(self.test_bank.get_requisite_objectives(self.dependent_objective.ident)), 2)
        for required_objective in self.test_bank.get_requisite_objectives(self.dependent_objective.ident):
            self.test_bank.unassign_objective_requisite(self.dependent_objective.ident, required_objective.ident)
        self.assertEqual(len(self.test_bank.get_requisite_objectives(self.dependent_objective.ident)), 0)

    def test_three_requisites_in_list(self):
        self.assertEqual(len(self.required_objectives), 3)


"""
class TestTypeLookup(unittest.TestCase):
    def setUp(self):
        tm = TypeManager()
        self.tas = tm.get_type_admin_session()
        self.tls = tm.get_type_lookup_session()
        self.test_type = Type(**{
            'authority': 'test-authority',
            'namespace': 'test-namespace',
            'identifier': 'test-identifier',
            'display_name': 'CRuD-Test Type',
            'display_label': 'CRuD',
            'description': 'This is a Test Type for CRuD',
            'domain': 'Test Domain'
        })
        if not self.tls.has_type(self.test_type):
            tfc = self.tas.get_type_form_for_create(self.test_type)
            tfc.display_name = 'CRuD-Test Type'
            tfc.display_label = 'CRuD-Test'
            tfc.description = 'This is a Test Type for CRuD'
            tfc.domain = 'Test Domain'
            self.tas.create_type(tfc)

    def test_get_types(self):
        from ..abstract_osid.type import primitives as abc_type_primitives
        for t in self.tls.get_types():
            self.assertTrue(isinstance(t, abc_type_primitives.Type))

    def test_get_type(self):
        from ..abstract_osid.type import primitives as abc_type_primitives
        type_to_get = self.tls.get_types().get_next_type()
        type_elements = {'identifier': type_to_get.identifier,
                         'namespace': type_to_get.namespace,
                         'authority': type_to_get.authority,}
        self.assertTrue(isinstance(self.tls.get_type(**type_elements),
                                   abc_type_primitives.Type))

    def tearDown(self):
        try:
            self.tas.delete_type(self.test_type)
        except:
            print 'Could not delete test type'
"""

class TestRepositoryLookup(unittest.TestCase):

    @unittest.skipIf(not rm.supports_repository_lookup(),
                     "repository lookup not supported in any underlying implementation")
    @unittest.skipIf(rm.get_repositories().available() == 0,
                     "there are no repositories available in any underlying implementation")
    def setUp(self):
        pass

    def test_repositories(self):
        all_repositories = rm.get_repositories()
        self.assertTrue(all_repositories.available() > 0)

    def test_get_repository(self):
        all_repositories = rm.get_repositories()
        self.assertTrue(all_repositories.available() > 0)
        for repository in all_repositories:
            self.assertTrue(isinstance(repository, abc_repository_objects.Repository))

    def test_get_repositories_by_ids(self):
        all_repositories = rm.get_repositories()
        all_repositories_count = all_repositories.available()
        id_list = []
        for repository in all_repositories:
            id_list.append(repository.get_id())
        self.assertTrue(len(id_list) == all_repositories_count)
        all_repositories_by_ids = rm.get_repositories_by_ids(id_list)
        self.assertTrue(all_repositories_by_ids.available() == all_repositories_count)
        for repository in all_repositories_by_ids:
            self.assertTrue(isinstance(repository, abc_repository_objects.Repository))

class TestRepositoryCrUD(unittest.TestCase):

    @unittest.skipIf(not rm.supports_repository_admin(),
                     "repository admin not supported in any underlying implementation")
    def setUp(self):
        self.default_repository_type = DEFAULT_TYPE

    def test_crud_repositories(self):
        # test create:
        rfc = rm.get_repository_form([])
        rfc.display_name = 'CrUD-Test Repository'
        rfc.description = 'This is a Test Repository for CrUD'
        new_repository = rm.create_repository(rfc)
        self.assertEqual(new_repository.display_name.text, 'CrUD-Test Repository')
        self.assertEqual(new_repository.description.text, 'This is a Test Repository for CrUD')

        # test update:
        new_repository_id = new_repository.get_id()
        rfu = rm.get_repository_form(new_repository_id)
        rfu.set_display_name('New Name for CrUD-Test Repository')
        rm.update_repository(rfu)
        updated_repository_id = new_repository_id
        self.assertEqual(rm.get_repository(updated_repository_id).display_name.text, 'New Name for CrUD-Test Repository')

        # test delete:
        rm.delete_repository(updated_repository_id)
        with self.assertRaises(NotFound):
            rm.get_repository(updated_repository_id)

    def test_truth(self):
        self.assertTrue(True)


class TestFamilyCrUD(unittest.TestCase):

    @unittest.skipIf(not relm.supports_family_admin(),
                     "family admin not supported in any underlying implementation")
    def setUp(self):
        self.default_family_type = DEFAULT_TYPE

    def test_crud_families(self):
        # test create:
        ffc = relm.get_family_form([])
        ffc.display_name = 'CrUD-Test Family'
        ffc.description = 'This is a Test Family for CrUD'
        new_family = relm.create_family(ffc)
        self.assertEqual(new_family.display_name.text, 'CrUD-Test Family')
        self.assertEqual(new_family.description.text, 'This is a Test Family for CrUD')

        # test update:
        new_family_id = new_family.get_id()
        ffu = relm.get_family_form(new_family_id)
        ffu.set_display_name('New Name for CrUD-Test Family')
        relm.update_family(ffu)
        updated_family_id = new_family_id
        self.assertEqual(relm.get_family(updated_family_id).display_name.text, 'New Name for CrUD-Test Family')

        # test delete:
        relm.delete_family(updated_family_id)
        with self.assertRaises(NotFound):
            relm.get_family(updated_family_id)

    def test_truth(self):
        self.assertTrue(True)


class TestAssetCrUD(unittest.TestCase):

    @unittest.skipIf(not rm.supports_repository_lookup(),
                     "repository lookup not supported in any underlying implementation")
    @unittest.skipIf(not rm.supports_repository_admin(),
                     "repository admin not supported in any underlying implementation")
    @unittest.skipIf(not rm.supports_asset_admin(),
                     "asset admin not supported in any underlying implementation")
    def setUp(self):
        try:
            test_repository = utilities.get_repository_by_name('Python Test Sandbox')
        except NotFound:
            test_repository = utilities.create_sandbox_repository('Python Test Sandbox', 'a sandbox catalog for testing Python implementations.')
        test_repo_id = test_repository.ident
        self.test_repo = rm.get_repository(test_repo_id)
        self.assertEqual(self.test_repo.get_display_name().get_text(), 'Python Test Sandbox')

    def test_crud_asset(self):
        # test create:
        afc = self.test_repo.get_asset_form([])
        afc.set_display_name('Test Asset')
        afc.set_description('This is a Test Asset')
        new_asset = self.test_repo.create_asset(afc)
        self.assertEqual(new_asset.get_display_name().get_text(), 'Test Asset')

        # test update:
        new_asset_id = new_asset.get_id()
        afu = self.test_repo.get_asset_form(new_asset_id)
        afu.set_display_name('New Name for Test Asset')
        self.test_repo.update_asset(afu)
        updated_asset_id = new_asset_id
        self.assertEqual(self.test_repo.get_asset(updated_asset_id).get_display_name().get_text(), 'New Name for Test Asset')

        # test delete:
        self.test_repo.delete_asset(new_asset_id)
        with self.assertRaises(NotFound):
            self.test_repo.get_asset(new_asset_id)

    def test_truth(self):
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()

